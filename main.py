import os
import sqlite3
import hashlib
from tabulate import tabulate

#main.py
#encoding: utf-8

__author__      = "Muhamad Zulfan Taqiyudin Baehaki", "Muhammad Rafif Fadlurahman"
__version__     = 1.0
__contact__     = {
                    "Rafif" :{
                        "Name"          :   "Muhammad Rafif Fadlurahman",
                        "Email"         :   "rafif.fadlurahman20@mhs.uinjkt.ac.id",
                        "University"    :   "UIN SYARIF HIDAYATULLAH JAKARTA"

                    },
                    "Zulfan":{
                        "Name"          :   "Muhamad Zulfan Taqiyudin Baehaki",
                        "Email"         :   "muhamad.zulfan20@mhs.uinjkt.ac.id",
                        "University"    :   "UIN SYARIF HIDAYATULLAH JAKARTA"
                    }    
} 

conn = sqlite3.connect("mesinkasir.db")

cur = conn.cursor()


cur.execute("""CREATE TABLE IF NOT EXISTS history(
    id INTEGER PRIMARY KEY,
    nama_barang TEXT NOT NULL,
    quantities INTEGER NOT NULL,
    total INTEGER NOT NULL,
    tanggal TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)""")

cur.execute("""CREATE TABLE IF NOT EXISTS income(
    id INTEGER PRIMARY KEY,
    income_history INT NOT NULL
)""")


cur.execute("""CREATE TABLE IF NOT EXISTS quantity(
    id INTEGER PRIMARY KEY,
    quantities INTEGER CHECK (quantities > -1) NOT NULL
)""")

cur.execute("""CREATE TABLE IF NOT EXISTS level(
    id INTEGER PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    passw TEXT NOT NULL
)""")

conn.commit()

# menu = {
#     1:  "Pensil",
#     2:  "Buku",
#     3:  "Bebek",
#     4: "buku gambar"
# }

# harga = {
#     1:  1_000,
#     2:  3_500,
#     3:  20_000,
#     4: 5000
# }

# input_kuantitas = (10, 15, 14, 1, 12, 2, 6, 8, 45, 23)


uname = 'd033e22ae348aeb5660fc2140aec35850c4da997'
passw = 'b67d212b8043863bcc65dc16d3419ee15961590a'



def get_quantities():
    with conn:
        cur.execute("SELECT quantities FROM quantity")
    return cur.fetchall()

kuantitas = get_quantities()

if kuantitas == []:
    cur.execute("""INSERT INTO quantity(quantities) VALUES(10),
    (15),
    (14),
    (1),
    (12),
    (2),
    (6),
    (8),
    (45),
    (23)""")
    conn.commit()


def update_quantities(id, kuantitas):
    with conn:
        cur.execute(f"UPDATE quantity set quantities = quantities - {kuantitas} where id = {id}")

def tambah_quantity(id, kuantitas):
    with conn:
        cur.execute(f"UPDATE quantity set quantities = quantities + {kuantitas} where id = {id}")


def cek_quantities(id):
    with conn:
        cur.execute(f"SELECT quantities from quantity where id = {id}")
    return cur.fetchall()

def insert_history(history):
    with conn:
        cur.execute("INSERT INTO history(nama_barang, quantities, total) VALUES(?, ?, ?)", history)

def get_history():
    with conn:
        cur.execute("SELECT * FROM history")
    return cur.fetchall()

def update_income(x):
    with conn:
        cur.execute(f"UPDATE income set income_history = income_history + {x} where id = 1 ")

def get_income():
    with conn:
        cur.execute("SELECT * FROM income")
    return cur.fetchall()

pemasukan = get_income()
if pemasukan == []:
    cur.execute("INSERT INTO income(income_history) VALUES(5000000)")
    conn.commit()
else:
    pass

def login():
    step = 3

    while(step > 0):
        print('login')
        usename = input('Username: ')
        password = input('Password: ')

        enc_usename = hashlib.sha1(usename.encode())
        enc_password = hashlib.sha1(password.encode())

        if( enc_password.hexdigest() == passw and enc_usename.hexdigest() == uname):
            return True
        else:
            pass

        step -= 1 
        if step > 0:
            print(f'kesempatan anda untuk login: {step}')
        else:
            print('gagal untuk login!')

    return False


def menuKita():
    print_product = product.items()
    print(tabulate(print_product, headers=["No", "Product"]))
    # print("+----------------------+")
    # print("|         menu         |")
    # print("+----------------------+")
    # for i  in range (1, len(product)+1):
    #     print(product[i][0] + "\t" + str(product[i][1]) + "\t"+ str(product[i][2]))
    # print("0.total")
    # for id, produk in product.items():
    #     print(tabulate('{}\t {}\t {} \t{}'.format(id, produk)))
    # # print(tabulate(product, headers=["Nama Barang", "Kuantitas", "Harga"]))

def write_struk(barang, q, harga):

    f = open('struk.txt', 'a')
    f.write(f'{barang}\t{q}\t{harga}\n')

    f.close()

def read_struk():

    f = open('struk.txt', 'r')

    print('\t======= struk belanja =======\n')
    print('nama barang\tkuantitas\tharga')
    print(f.read())

    f.close()

    if os.path.exists('struk.txt'):
        os.remove('struk.txt')
    else:
        print('The File is not exists')

def payment():

    data_kuantitas = get_quantities()

    product = {
        1 : ["Bola Voli", data_kuantitas[0][0], 50_000],
        2 : ["Bola Futsal", data_kuantitas[1][0], 55_000],
        3 : ["Bola Kasti", data_kuantitas[2][0], 10_000],
        4 : ["Raket Badinton", data_kuantitas[3][0], 300_000],
        5 : ["Shuttle Cock", data_kuantitas[4][0], 90_000],
        6 : ["Jump Rope", data_kuantitas[5][0], 30_000],
        7 : ["Senar Raket", data_kuantitas[6][0], 100_000],
        8 : ["Hand Grip", data_kuantitas[7][0], 5_000],
        9 : ["Kaos Kaki", data_kuantitas[8][0], 10_000],
        10 : ["Baju Jersey", data_kuantitas[9][0], 85_000]
    }
    
    print("No\tNama Product\tStock\tHarga")
    for i  in range (1, len(product)+1):
        print(str(i)+"\t" + product[i][0] + "\t" + str(product[i][1]) + "\t"+ str(product[i][2]))

    # for row in zip(*([key] + (value) for key, value in product.items())):
    #     print(*row)

    # for id, produk in product.items():
    #     print('{}\t {}'.format(id, produk))
        
    # print_product = sorted(product.items())
    # print(tabulate(print_product, headers=["No", "Product"]))

    print("0. Total/Bayar")
    total = 0
    buffer = 0
    x = 1
    while(x==1):
        quantity = 0
        inputMenu= 0
        while(True):
            inputMenu = int(input("Input:"))
            if inputMenu >10:
                print("index menu berada pada range 1 - 10")
                continue
            elif inputMenu < 0:
                print("index menu berada pada range 1 - 10")
                continue
            else:
                break
        
        if (inputMenu == 0):
            if(total == 0):
                print("Belum ada barang yang di beli")
                continue
            read_struk()
            print("total harga:" + str(total))
            break
        elif (inputMenu > 0 or inputMenu < 11):
            validasi_stock_barang = cek_quantities(inputMenu)
            validasi = validasi_stock_barang[0][0]

            if(validasi < 1):
                print("stock barang tidak tersedia!")
                continue

            barang = product[inputMenu][0]
            print(product[inputMenu][0] + "\t harga: " + str(product[inputMenu][2]))
                
            while(True):
                cek_kuantitas = cek_quantities(inputMenu)
                quantity= int(input("quantity: "))

                print(type(cek_kuantitas[0][0]))
                buffer_cek = cek_kuantitas[0][0]

                buffer_cek -= quantity
                if(buffer_cek < 0):
                    print("Kuantitas barang tidak tersedia!\nbarang hanya memiliki kuantitas: "+ str(cek_kuantitas[0][0]+"\nsilakan input ulang kuantitas..\n "))
                else:
                    break

            update_quantities(inputMenu, quantity)
            buffer = product[inputMenu][2] * quantity

            update_history = (barang, quantity, buffer)
            insert_history(update_history)
            write_struk(barang, quantity, buffer)

        else:
            print("wrong input code!")

        total += buffer
    return total

def bayar():
    total = payment()
    update_income(total)
    x = True
    while x is True:
        uangAnda = int(input("masukkan uang anda: "))
        if uangAnda >= total:
            x = False
        elif(uangAnda < total):
            print("Uang anda tidak cukup!")
    
    kembalian = total - uangAnda
    if(kembalian == 0 ):
        print("Uang anda pas!")
    elif(kembalian > 0 ):
        print(f"kembalian anda: {kembalian}")
    
    print("\n")

    print("ingin belanja lagi?(y/n)")
    ulangi = str(input("y untuk ya, dan n untuk tidak: "))
    if(ulangi == 'y'):
        os.system("cls")
        bayar()
    elif(ulangi == 'n'):
        wait = input("press anything to continue....")
        os.system("cls")
        run()

def history():
    history_print = get_history()
    print(tabulate(history_print, headers=["No", "Nama Produk", "Kuantitas", "Harga", "Tanggal"]))
    wait = input("press enter to back to menu...")
    os.system("cls")
    run()

def income():
    income_print = get_income()
    print(tabulate(income_print, headers=["No", "Income"]))
    wait = input("press enter to back to menu...")
    os.system("cls")
    run()

def update_quantity():

    global product

    print("No\tNama Product\tStock\tHarga")
    for i  in range (1, len(product)+1):
        print(str(i)+"\t" + product[i][0] + "\t" + str(product[i][1]) + "\t"+ str(product[i][2]))
    
    update_id = 0
    while(True):
            print('masukan id produk yang akan di update_id kuantitasnya ')
            update_id = int(input("Input:"))
            if update_id >10:
                print("index menu berada pada range 1 - 10")
                continue
            elif update_id < 1:
                print("index menu berada pada range 1 - 10")
                continue
            else:
                break
       
    print(product[update_id][0] + "\tKuantitas: "   +  str(product[update_id][1]))

    update_stok = int(input('inputkan kuantitas yang akan ditambahkan: '))

    tambah_quantity(update_id, update_stok)

    print('data berhasil di update!')
    next = input('press anything to back to menu...')
    os.system('cls')
    run()

def run():



    print("====== TOKO SPORT ZURA  =======")
    print("1. Payment")
    print("2. History")
    print("3. Income")
    print('4. Update Quantity')
    print("0. quit")
    pilih = str(input("input: "))

    if(pilih == "1"):
        os.system('cls')
        bayar()
    elif(pilih == "2"):
        akses = login()
        if akses == True:
            os.system('cls')
            history()
        elif akses == False:
            tunggu = input('anda tidak dapat mengakses menu history!\npencet apasaja untuk kembali ke menu')
            os.system('cls')
            run()
       
    elif pilih == "3":
        akses = login()
        if akses == True:
            os.system('cls')
            income()
        elif akses == False:
            tunggu = input('anda tidak dapat mengakses menu income!\npencet apasaja untuk kembali ke menu')
            os.system('cls')
            run()
    elif pilih == "4":
        akses = login()
        if akses == True:
            os.system('cls')
            update_quantity()
        elif akses == False:
            tunggu = input('anda tidak dapat mengakses update quantity!\npencet apasaja untuk kembali ke menu')
            os.system('cls')
            run()
    elif(pilih == "0"):
        conn.close()
        quit()
    else:
        os.system('cls')
        run()
     
data_kuantitas = get_quantities()
product = {
    1 : ["Bola Voli", data_kuantitas[0][0], 50_000],
    2 : ["Bola Futsal", data_kuantitas[1][0], 55_000],
    3 : ["Bola Kasti", data_kuantitas[2][0], 10_000],
    4 : ["Raket Badinton", data_kuantitas[3][0], 300_000],
    5 : ["Shuttle Cock", data_kuantitas[4][0], 90_000],
    6 : ["Jump Rope", data_kuantitas[5][0], 30_000],
    7 : ["Senar Raket", data_kuantitas[6][0], 100_000],
    8 : ["Hand Grip", data_kuantitas[7][0], 5_000],
    9 : ["Kaos Kaki", data_kuantitas[8][0], 10_000],
    10 : ["Baju Jersey", data_kuantitas[9][0], 85_000]
    }       
run()   